/* Cortex M0 core interrupt handlers */
void Reset_Handler(void);
void NMI_Handler(void) __attribute__ ((weak, alias ("Dummy_Handler")));
void HardFault_Handler(void) __attribute__ ((weak, alias ("Dummy_Handler")));

void Dummy_Handler(void) {
    while (1);
}

extern int main(void);

extern unsigned int _end_text;
extern unsigned int _start_data;
extern unsigned int _end_data;
extern unsigned int _start_bss;
extern unsigned int _end_bss;

void Reset_Handler(void)
{
   unsigned int *src, *dst;

   /* Copy data section from flash to RAM */
   src = &_end_text;
   dst = &_start_data;
   while (dst < &_end_data)
       *dst++ = *src++;

   /* Clear the bss section */
   dst = &_start_bss;
   while (dst < &_end_bss)
       *dst++ = 0;

   /* Our program entry ! */
   main();
}


extern unsigned int _end_stack;
void *vector_table[] __attribute__ ((section(".vectors"))) = {
   &_end_stack, /* Initial SP value */ /* 0 */
   Reset_Handler,
   NMI_Handler,
   HardFault_Handler,
   0,
   0, /* 5 */
   0,
   /* Entry 7 (8th entry) must contain the 2’s complement of the check-sum
      of table entries 0 through 6. This causes the checksum of the first 8
      table entries to be 0 */
   (void *)0xDEADBEEF, /* Actually, this is done using an external tool. */
   0,
   /* [.....] voir le code du module GPIO-Demo pour la suite */
};
