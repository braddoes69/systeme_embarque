> Ilias Redessi

> Emilien Nicolas

# Compte rendu TP #

## TP Prise en main ##

> **Question** : Rechercher les caractéristiques des diverses cartes en question et les micro-contrôleurs utilisés par chacune d'entre elle.

- Carte C8051F02x de SiliconLabs : 8051 8-bit
- Carte Arduino Uno : ATmega328 8-bit
- Carte STM32 de ST Micro-electronics : STM32
- Module GPIO-Démo de Techno-Innov : NXP’s LPC1224

> Quelle est ou quelles sont cette (ou ces) chaîne(s) de cross-compilation ? Comment l'utilise-t-on ?

Il permet de compiler un projet dont l'architecture final et différente de l'architecture source.
on va utiliser le binaire gcc correspondant (arm-linux-gnueabi-gcc par exemple)

> Utilisez l'option -v de gcc (de la chaîne de cross-compilation) pour identifier la version et vérifier qu'il s'exécute correctement.

Version 7.2.0

> Quelles sont les diférences entre ces trois chaînes de compilation croisée ?

C'est en fonction de l'architecture

## TP From Scratch ##

> **Question** : Quelle est la valeur à utiliser pour la variable CROSS_COMPILE pour le cas d'un micro-contrôleur comme le LPC1224 ?

arm-none-eabi

## TP Quelque drivers ##

> **Question** : Testez ce code. Que se passe t-il ?

La led est rouge et ne clignote pas. C'est comme si le code n'avait toggle la led qu'une seule fois

## TP utilisation ##

> Le protocole défini est à mettre dans le rapport, avec une courte explication des choix réalisés.
Proposez aussi une version "binaire" avec somme de contrôle, mais restant proche de ce que vous avez défini, pour laquelle seul le format des données numériques change.

CLIQUE DROIT + générer le protocole

on a un caractère pour définir la clé (D: direction, A: accélération, P: phare, U: ultrason) et un octet de valeur
